<?php

/**
 * @file
 * Install file for erpal_feedback_reloaded.
 */

/**
 * Implements hook_install().
 */
function erpal_feedback_reloaded_install() {
  $t = get_t();

  $entity = entity_create('feedback_reloaded_type', array(
    'type' => 'erpal_feedback',
    'name' => 'erpal_feedback',
    'label' => 'ERPAL feedback',
    'description' => 'This is ERPAL feedback type.',
  ));

  feedback_reloaded_type_save($entity);

  // Add field_erpal_feedback_nid.
  $field = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_erpal_feedback_nid',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );
  if (!field_info_field('field_erpal_feedback_nid')) {
    field_create_field($field);
  }

  $instance = array(
    'field_name' => 'field_erpal_feedback_nid',
    'entity_type' => 'feedback_reloaded',
    'bundle' => 'erpal_feedback',
    'label' => $t('Nid'),
    'required' => FALSE,
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );
  if (!field_info_instance('feedback_reloaded', 'field_erpal_feedback_nid', 'erpal_feedback')) {
    field_create_instance($instance);
  }

  // Add field_erpal_feedback_priority.
  $field = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_erpal_feedback_priority',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );
  if (!field_info_field('field_erpal_feedback_priority')) {
    field_create_field($field);
  }

  $instance = array(
    'field_name' => 'field_erpal_feedback_priority',
    'entity_type' => 'feedback_reloaded',
    'bundle' => 'erpal_feedback',
    'label' => $t('Priority'),
    'required' => TRUE,
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );
  if (!field_info_instance('feedback_reloaded', 'field_erpal_feedback_priority', 'erpal_feedback')) {
    field_create_instance($instance);
  }

  // Add field_erpal_feedback_status.
  $field = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_erpal_feedback_status',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );
  if (!field_info_field('field_erpal_feedback_status')) {
    field_create_field($field);
  }

  $instance = array(
    'field_name' => 'field_erpal_feedback_status',
    'entity_type' => 'feedback_reloaded',
    'bundle' => 'erpal_feedback',
    'label' => $t('Status'),
    'required' => TRUE,
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );
  if (!field_info_instance('feedback_reloaded', 'field_erpal_feedback_status', 'erpal_feedback')) {
    field_create_instance($instance);
  }

  // Add field_erpal_feedback_type.
  $field = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_erpal_feedback_type',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );
  if (!field_info_field('field_erpal_feedback_type')) {
    field_create_field($field);
  }

  $instance = array(
    'field_name' => 'field_erpal_feedback_type',
    'entity_type' => 'feedback_reloaded',
    'bundle' => 'erpal_feedback',
    'label' => $t('Type'),
    'required' => TRUE,
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );
  if (!field_info_instance('feedback_reloaded', 'field_erpal_feedback_type', 'erpal_feedback')) {
    field_create_instance($instance);
  }
}

/**
 * Implements hook_uninstall().
 */
function erpal_feedback_reloaded_uninstall() {

  variable_del('erpal_feedback_url');
  variable_del('erpal_feedback_login');
  variable_del('erpal_feedback_password');
  variable_del('erpal_feedback_task_body');
  variable_del('erpal_feedback_ticket');
  variable_del('erpal_feedback_project_nid');
  variable_del('erpal_feedback_task_nid');

  // If Feedback Reloaded module already was disabled, then
  // we can't completely delete feedbacks from it.
  if (!module_exists('feedback_reloaded')) {
    return;
  }

  // Delete all entries of Feedback Reloaded module.
  $feedbacks = db_select('feedback_reloaded', 'fr')
    ->fields('fr', array('fid'))
    ->condition('fr.type', 'erpal_feedback')
    ->execute();

  $fids = array();
  foreach ($feedbacks as $feedback) {
    $fids[] = $feedback->fid;
  }

  feedback_reloaded_delete_multiple($fids);

  // Delete entity type provided by this module.
  $entity = feedback_reloaded_type_load('erpal_feedback');
  feedback_reloaded_type_delete($entity);
}

/**
 * Implements hook_enable().
 */
function erpal_feedback_reloaded_enable() {

  // Set method of making a screenshots that doesn't requires any time
  // consuming settings.
  variable_set('feedback_reloaded_screenshot_method', 'html2canvas');

  // Ensure that translations are available.
  $t = get_t();
  drupal_set_message($t('ERPAL feedback reloaded module was enabled. Please set you ERPAL settings on <a href="/admin/config/workflow/feedback/erpal_feedback">config page</a>.'), 'warning');
}
